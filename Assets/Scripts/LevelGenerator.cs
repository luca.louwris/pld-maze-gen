﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEditor.PackageManager;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

public enum TileType {
    Empty = 0,
    Player,
    Enemy,
    Wall,
    Door,
    Key,
    Dagger,
    End
}

public class LevelGenerator : MonoBehaviour
{
    [Range(64,128)]public int LevelWidth, LevelHeight;
    public GameObject[] Tiles;
    [Range(2,500)] public int MaxDepth = 1;
    
    [Header("Dungeon Generation variable")]
    [SerializeField] private int chanceToInit = 30;
    [SerializeField] private int birthLimit = 3;
    [SerializeField] private int deathLimit = 3;
    
    
    [SerializeField] private int amountOfEnemies = 3;
    
    private bool[,] fillMap;

    private int horizontalWallPos;
    private int verticalWallPos;

    protected void Start()
    {        
        // Making sure that the level has a even width and height.
        LevelHeight = LevelHeight - LevelHeight % 2;
        LevelWidth = LevelWidth - LevelWidth % 2;
        int width = LevelWidth;
        int height = LevelHeight;
        
        TileType[,] grid = new TileType[height, width];
        fillMap = new bool[height, width];

        // Base level
        // FillBlock(grid, 0, 0, width, height, TileType.Wall);
        // FillBlock(grid, 26, 26, 12, 12, TileType.Empty);
        // FillBlock(grid, 32, 28, 1, 1, TileType.Player);
        // FillBlock(grid, 30, 30, 1, 1, TileType.Dagger);
        // FillBlock(grid, 34, 30, 1, 1, TileType.Key);
        // FillBlock(grid, 32, 32, 1, 1, TileType.Door);
        // FillBlock(grid, 32, 36, 1, 1, TileType.Enemy);
        // FillBlock(grid, 32, 34, 1, 1, TileType.End);
        
        // Starting on clean plate
        FillBlock(grid, 0,0,width,height, TileType.Wall);

        // Applying recursive 
        FillRandom(grid, 1, 1, width - 1, height - 1, TileType.Empty);
        horizontalWallPos = (int)Random.Range(height * 0.3f, height * 0.7f);
        verticalWallPos = (int)Random.Range(width * 0.3f, width * 0.7f);
        FillBlock(grid, verticalWallPos,0,2,height, TileType.Wall);
        FillBlock(grid, 0,horizontalWallPos,width,2, TileType.Wall);
        
        ProcessArray(grid, 0);
        
        FillSquare(grid, 0, 0, width, height, TileType.Wall);

        CheckWalls(grid, horizontalWallPos, true);
        CheckWalls(grid, verticalWallPos, false);
        
        
        // Debugger.instance.AddLabel(32, 26, "Room 1");
        
        //use 2d array (i.e. for using cellular automata) if fill completed.
        if(Fill(grid, new Vector2(1,1),new Vector2(verticalWallPos, horizontalWallPos)))
            CreateTilesFromArray(grid);
    }

    private void CheckWalls(TileType[,] grid, int wall, bool horizontal)
    {
        if (horizontal)
        {
            for (int x = 0; x < LevelWidth; x++)
            {
                if (grid[wall + 2, x] != TileType.Wall && grid[wall - 1, x] != TileType.Wall)
                {
                    grid[wall + 1, x] = TileType.Empty;
                    grid[wall, x] = TileType.Door;

                    x += 20;
                }
                    
            }
        }
        else
        {
            for (int y = 0; y < LevelHeight; y++)
            {
                if (grid[y, wall + 2] != TileType.Wall && grid[y, wall - 1] != TileType.Wall)
                {
                    grid[y, wall + 1] = TileType.Empty;
                    grid[y, wall] = TileType.Door;

                    y += 20;
                }
                    
            }
        }
    }

    private void FillRandom(TileType[,] grid, int xPos, int yPos, int width, int height, TileType fillType)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if(Random.Range(0,100) < chanceToInit)
                    grid[y + yPos, x + xPos] = fillType;
            }
        }
    }
    
    private TileType[,] ProcessArray(TileType[,] grid, int depth)
    {
        depth++;
        if (MaxDepth < depth) return grid;
        
        TileType[,] gridDeeper = grid;
        for (int y = 1; y < grid.GetLength(0) - 1; y++)
        {
            for (int x = 1; x < grid.GetLength(1) - 1; x++)
            {
                int aliveNeighbours = 0;
                if (grid[y, x] != TileType.Wall) aliveNeighbours++;
                if (grid[y + 1, x] != TileType.Wall) aliveNeighbours++;
                if (grid[y - 1, x] != TileType.Wall) aliveNeighbours++;
                if (grid[y, x + 1] != TileType.Wall) aliveNeighbours++;
                if (grid[y, x - 1] != TileType.Wall) aliveNeighbours++;
                if (grid[y + 1, x - 1] != TileType.Wall) aliveNeighbours++;
                if (grid[y - 1, x + 1] != TileType.Wall) aliveNeighbours++;
                if (grid[y + 1, x + 1] != TileType.Wall) aliveNeighbours++;
                if (grid[y - 1, x - 1] != TileType.Wall) aliveNeighbours++;
                
                if (grid[y, x ] == TileType.Wall && aliveNeighbours > birthLimit)
                {
                    gridDeeper[y, x] = TileType.Empty;
                }
                if (grid[y, x] == TileType.Empty && aliveNeighbours < deathLimit)
                {
                    gridDeeper[y, x] = TileType.Wall;
                }
            }
        }
        
        return ProcessArray(gridDeeper, depth);
    }

    private bool Fill(TileType[,] grid, Vector2 startPos, Vector2 maxPos)
    {
        Queue<Vector2> list = new Queue<Vector2>();
        List<Vector2> checkedTiles = new List<Vector2>();
        List<Vector2> allChecked = new List<Vector2>();
        
        int amountOfRooms = 0;
        
        // Pick random position from from array
        int x, y, timesRun = 0;
        do
        {
            y = Random.Range((int)startPos.y, (int) maxPos.y);
            x = Random.Range((int)startPos.x, (int) maxPos.x);
        } while (grid[y, x] == TileType.Wall && !fillMap[y, x]);

        list.Enqueue(new Vector2(x, y));
        
        while (list.Count > 0)
        {
            Vector2 pos = list.Dequeue();
            
            // Checking if it's out of bounds
            if (pos.x < 1 || pos.y < 1 || pos.x > LevelWidth - 1 || pos.y > LevelHeight - 1) continue;
            // Checking if it's checked
            if (fillMap[(int) pos.y, (int) pos.x]) continue;
            // Checking if it's a wall
            if (grid[(int) pos.y, (int) pos.x] == TileType.Wall) continue;
            
            checkedTiles.Add(pos);
            
            if (grid[(int) pos.y, (int) pos.x] == TileType.Door)
            {
                amountOfRooms++;
                
                grid[(int)checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.8f)].y, (int) checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.8f)].x] = TileType.Enemy;
                grid[(int)checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.1f)].y, (int) checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.1f)].x] = TileType.Dagger;

                grid[(int)checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.4f)].y, (int) checkedTiles[Mathf.RoundToInt( checkedTiles.Count * 0.4f)].x] = TileType.Key;
                
                allChecked.AddRange(checkedTiles);
                checkedTiles.Clear();
            };
            
            timesRun++;

            fillMap[(int) pos.y, (int) pos.x] = true;

            list.Enqueue(new Vector2(pos.x + 1, pos.y));
            list.Enqueue(new Vector2(pos.x - 1, pos.y));
            list.Enqueue(new Vector2(pos.x, pos.y + 1));
            list.Enqueue(new Vector2(pos.x, pos.y - 1));
        }
        
        if (allChecked.Count < (LevelWidth*LevelHeight)/5)
        {
            Debug.Log("Not enough room");
            allChecked.Clear();
            
            GameManager manager = FindObjectOfType<GameManager>();
            
            manager.OnDone();
            manager.OnReset();

            return false;
        }
        
        grid[(int) allChecked[4].y, (int) allChecked[4].x] = TileType.Player;
        
        grid[(int) allChecked.Last().y, (int) allChecked.Last().x] = TileType.End;
        return true;
        
    }

    /// Fill solid square of array with tiles
    private void FillBlock(TileType[,] grid, int x, int y, int width, int height, TileType fillType) {
        for (int tileY=0; tileY<height; tileY++) {
            for (int tileX=0; tileX<width; tileX++) {
                grid[tileY + y, tileX + x] = fillType;
            }
        }
    }
    
    /// Fill hollow square of array with tiles
    private void FillSquare(TileType[,] grid, int x, int y, int width, int height, TileType fillType) {
        // draw lines over the whole width on start point y
        for (int i = 0; i < height; i++)
        { grid[i, x] = fillType; }
        for (int i = 0; i < height; i++)
        { grid[i, x + width - 1] = fillType; }
        for (int i = 0; i < width; i++)
        { grid[y, i] = fillType; }
        for (int i = 0; i < width; i++)
        { grid[y + height - 1, i] = fillType; }
    }

    /// use array to create tiles
    private void CreateTilesFromArray(TileType[,] grid) {
        int height = grid.GetLength(0);
        int width = grid.GetLength(1);
        for (int y=0; y<height; y++) {
            for (int x=0; x<width; x++) {
                 TileType tile = grid[y, x];
                 if (tile != TileType.Empty) {
                     CreateTile(x, y, tile);
                 }
            }
        }
    }

    //create a single tile
    private GameObject CreateTile(int x, int y, TileType type) {
        int tileID = ((int)type) - 1;
        if (tileID >= 0 && tileID < Tiles.Length)
        {
            GameObject tilePrefab = Tiles[tileID];
            if (tilePrefab != null) {
                GameObject newTile = GameObject.Instantiate(tilePrefab, new Vector3(x, y, 0), Quaternion.identity);
                newTile.transform.SetParent(transform);
                return newTile;
            }

        } else {
            Debug.LogError("Invalid tile type selected");
        }

        return null;
    }

}